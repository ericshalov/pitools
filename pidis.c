/*
	pidis.c
	Raspberry Pi (ARM) disassembler
	(C) Copyright 2013-2014 Eric Shalov. All Rights Reserved.

	The Raspberry Pi integrates a ARM1176JZFS CPU in it's Broadcom
	BCM2835 SoC.
	This 1176 ARM1176JZ-S processor features ARM TrustZone technology
	for secure applications and ARM Jazelle technology for efficient
	embedded Java execution.
	It's an ARMv6-compatible processor rev 7 (v6l)

	The ARM on the Pi is described in the ARM-v7AR A.R.M. ("ARM
	Architecture Reference Manual, ARMv7-A and ARMv7-R edition", ARM
	reference ARM DDI 0406).  The ARM-v7M ARM is no use.  The three
	subfamilies of ARM are A, R & M.
	It's using instruction encoding A1.
	
	Vector Floating Point coprocessor for automotive/industrial controls
	and 3D graphics acceleration.
	http://www.arm.com/products/processors/classic/arm11/index.php

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the {organization} nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>

#define S_EQ(x,y) (strcmp(x,y)==0)

#define BIT(x,b) ((x>>b) & 1)

/* prototypes */
void decode_armv6(unsigned long word);
void decode_thumb_halfword(unsigned short s);

/* thirteen general-purpose32-bit registers, R0 to R12, three 32-bit
   registers, R13 to R15, that sometimes or always have a special use.
	SP, the Stack Pointer = pointer to the active stack
	LR, the Link Register = stores the return address from a subroutine
	PC, the Program Counter = the address of the current instruction plus 8
	(Linux seems to use r11 as fp)
*/
char *reg_names[] = {"r0", "r1", "r2",  "r3",  "r4",  "r5", "r6", "r7",
		     "r8", "r9", "r10", "fp", "r12", "sp", "lr", "pc"};

char *shift_types[] = {"lsl","lsr", "asr", "ror"};

char *condition_suffixes[] = {
	"eq", "ne", "cs", "cc", "mi", "pl", "vs", "vc",
	"hi", "ls", "ge", "lt", "gt", "le",  ""
};

#define MAX_BRANCHES 4096
unsigned long num_branches = 0;
unsigned long branch[MAX_BRANCHES];

unsigned long offset = 0x8074;

int main(int argc, char *argv[]) {
	FILE *f;
	unsigned long word;
	int i;

	if(argc != 2) {
		fprintf(stderr,"Usage: %s [filename]\n", argv[0]);
		exit(1);
	}

	if( (f=fopen(argv[1],"r")) ) {
		while( fread(&word, 4, 1, f) ) {
			printf("%08lx: %08lx ", offset, word);
			decode_armv6(word);
			printf("\n");
			offset += 4;
		}
		fclose(f);
	} else {
		fprintf(stderr,"Unable to open `%s'.\n", argv[1]);
		exit(1);
	}


	for(i=0;i<num_branches;i++) printf(".L%d = %08lx\n", i, branch[i]);

	return 0;
}

/* A8.6.1 ADC (immediate) */
void decode_adc_imm(unsigned long w) {
	printf("adc%s   %s, %s, #%ld", condition_suffixes[w>>28],
                reg_names[(w>>12) & 0xF],
                reg_names[(w>>16) & 0xF],
                w&0xFFF
        );
}

/* A8.6.2 ADC (register) */
void decode_adc_reg(unsigned long w) {
	if(((w>>4) & 0x1)==0) {
		printf("add%c%s  %s, %s, %s, %lx",
			((w>>20)&0x1) ? 's':' ', condition_suffixes[w>>28],
			reg_names[(w>>12) & 0xF],
			reg_names[(w>>16) & 0xF],
			reg_names[w&0xF],
			(w>>7)&0x1F
		);
	} else {
		/* A8.6.3 ADC (register-shifted register) */
		printf("add%c%s  %s, %s, %s, %s %s",
			((w>>20)&0x1) ? 's':' ', condition_suffixes[w>>28],
			reg_names[(w>>12) & 0xF],
			reg_names[(w>>16) & 0xF],
			reg_names[w&0xF],
			shift_types[(w>>5) & 0x03],
			reg_names[(w>>8) & 0xF]
		);
	}
}


/* A8.6.5 ADD (immediate, ARM) */
void decode_add_imm(unsigned long w) {
        printf("add%s   %s, %s, # %lx", condition_suffixes[w>>28],
                reg_names[(w>>12) & 0xF],
                reg_names[(w>>16) & 0xF],
                w&0xFFF
        );
}

/* A8.6.6 ADD (register) */
void decode_add_reg(unsigned long w) {
	if(((w>>4) & 0x1)==0) {
		printf("add%c%s  %s, %s, %s, %lx",
			((w>>20)&0x1) ? 's':' ', condition_suffixes[w>>28],
			reg_names[(w>>12) & 0xF],
			reg_names[(w>>16) & 0xF],
			reg_names[w&0xF],
			(w>>7)&0x1F
		);
	} else {
		/* A8.6.7 ADD (register-shifted register) */
		printf("add%c%s  %s, %s, %s, %s %s",
			((w>>20)&0x1) ? 's':' ', condition_suffixes[w>>28],
			reg_names[(w>>12) & 0xF],
			reg_names[(w>>16) & 0xF],
			reg_names[w&0xF],
			shift_types[(w>>5) & 0x03],
			reg_names[(w>>8) & 0xF]
		);
	}
}

/* A8.6.8 ADD (SP plus immediate) */
void decode_add_sp_plus_imm(unsigned long w) {
	printf("add%c%s  %s, sp, # %ld",
		((w>>20)&0x1) ? 's':' ', condition_suffixes[w>>28],
		reg_names[(w>>12) & 0xF],
		w&0xFFF
	);
}

/* A8.6.9 ADD (SP plus register) */
void decode_add_sp_plus_reg(unsigned long w) {
	printf("add%c%s  %s, sp, %s, # %ld",
		((w>>20)&0x1) ? 's':' ', condition_suffixes[w>>28],
		reg_names[(w>>12) & 0xF],
		reg_names[w& 0xF],
		(w>>7)&0x1F
	);
}

/* A8.6.10 ADR: Encoding A1 */
void decode_adr(unsigned long w) {
	printf("adr%s   %s, %lx", condition_suffixes[w>>28],
		reg_names[(w>>12) & 0xF],
		w&0xFFF
	);
}

/* A8.6.10 ADR: Encoding A2 */
void decode_adr_pc_zero(unsigned long w) {
	if( (w&0xFFF) == 0) {
		printf("sub%s   %s, pc, #0", condition_suffixes[w>>28],
			reg_names[(w>>12) & 0xF]
		);
	} else {
		printf("adr%s   %s, %-lx", condition_suffixes[w>>28],
			reg_names[(w>>12) & 0xF],
			w&0xFFF
		);
	}
}

/* A8.6.11 AND (immediate) */
void decode_and_imm(unsigned long w) {
	printf("%c%s  %s, %s, #%ld",
		((w>>20)&0x1) ? 's':' ', condition_suffixes[w>>28],
		reg_names[(w>>12) & 0xF],
		reg_names[(w>>16) & 0xF],
		w&0xFFF
	);

}

/* A8.6.12 AND (register) */
void decode_and_reg(unsigned long w) {
	if(((w>>4) & 0x1)==0) {
		printf("add%c%s  %s, %s, %s, %lx",
			((w>>20)&0x1) ? 's':' ', condition_suffixes[w>>28],
			reg_names[(w>>12) & 0xF],
			reg_names[(w>>16) & 0xF],
			reg_names[w&0xF],
			(w>>7)&0x1F
		);
	} else {
		/* A8.6.13 AND (register-shifted register) */
		printf("add%c%s  %s, %s, %s, %s %s",
			((w>>20)&0x1) ? 's':' ', condition_suffixes[w>>28],
			reg_names[(w>>12) & 0xF],
			reg_names[(w>>16) & 0xF],
			reg_names[w&0xF],
			shift_types[(w>>5) & 0x03],
			reg_names[(w>>8) & 0xF]
		);
	}
}

/* A8.6.14 ASR (immediate) */
void decode_asr_imm(unsigned long w) {
        printf("asr%c%s   %s, %s, # %lx",
		((w>>20)&0x1) ? 's':' ', condition_suffixes[w>>28],
                reg_names[(w>>12) & 0xF],
                reg_names[w&0xF],
                (w>>7)&0x1F
        );
}

/* A8.6.15 ASR (register) */
void decode_asr_reg(unsigned long w) {
	printf("asr%c%s   %s, %s, %s",
		((w>>20)&0x1) ? 's':' ', condition_suffixes[w>>28],
		reg_names[(w>>12) & 0xF],
		reg_names[w&0xF],
		reg_names[(w>>8) & 0xF]
	);
}

/* A8.6.16 B */
void decode_b(unsigned long w) {
	printf("b%s     %lx <main+0x%lx>", condition_suffixes[w>>28],
		(w & 0x800000) ? 8+offset+4*((signed long) (0xFF000000|(w&0xFFFFFF))) : 8+offset+4*(w & 0xFFFFFF),
		(w & 0x800000) ? 8+       4*((signed long) (0xFF000000|(w&0xFFFFFF))) : 8+       4*(w & 0xFFFFFF)
	);
	branch[num_branches++] = offset;
}

/* A8.6.17 BFC */
void decode_bfc(unsigned long w) {
	printf("bfc%s   %s, #%ld, #%ld", condition_suffixes[w>>28],
		reg_names[(w>>12) & 0xF],
		(w>>7)&0x1F,
		(w>>16)&0x1F
	);
}

/* A8.6.18 BFI */
void decode_bfi(unsigned long w) {
	printf("bfi%s   %s, %s, #%ld, #%ld", condition_suffixes[w>>28],
		reg_names[(w>>12) & 0xF],
		reg_names[w&0xF],
		(w>>7)&0x1F,
		(w>>16)&0x1F
	);
}

/* A8.6.19 BIC (immediate) */
void decode_bic_imm(unsigned long w) {
	printf("bic%c%s  %s, %s, #%ld",
		((w>>20)&0x1) ? 's':' ', condition_suffixes[w>>28],
		reg_names[(w>>12) & 0xF],
		reg_names[(w>>16)&0xF],
		w&0xFFF
	);
}

/* A8.6.20 BIC (register) */
void decode_bic_reg(unsigned long w) {
	printf("bic%c%s  %s, %s, %s, %s #%ld",
		((w>>20)&0x1) ? 's':' ', condition_suffixes[w>>28],
		reg_names[(w>>12) & 0xF],
		reg_names[(w>>16)&0xF],
		reg_names[w&0xF],
		shift_types[(w>>5) & 0x03],
		(w>>7)&0x1F
	);
}

/* A8.6.21 BIC (register-shifted register) */
void decode_bic_reg_shift_reg(unsigned long w) {
	printf("bic%c%s  %s, %s, %s, %s %s",
		((w>>20)&0x1) ? 's':' ', condition_suffixes[w>>28],
		reg_names[(w>>12) & 0xF],
		reg_names[(w>>16)&0xF],
		reg_names[w&0xF],
		shift_types[(w>>5) & 0x03],
		reg_names[(w>>8)&0xF]
	);
}

/* A8.6.22 BKPT */
void decode_bkpt_imm(unsigned long w) {
	/* BKPT instruction must be unconditional */
	printf("bkpt%s #%ld", condition_suffixes[w>>28],
		(w>>8)&0xFFF
	);
}

/* A8.6.23 BL, BLX (immediate) */
void decode_bl_imm(unsigned long w) {
	printf("bl%c%s     %lx",
		((w>>28)&0xF)==0xF ? 'x' : ' ', condition_suffixes[w>>28],
		w & 0x1FFFFFF /* needs SignExtend() */
	);
}

/* A8.6.24 BLX (register) */
void decode_blx_reg(unsigned long w) {
	printf("blx%s    %s",
		condition_suffixes[w>>28],
		reg_names[w&0xF]
	);
}

/* A8.6.25 BX */
void decode_bx_reg(unsigned long w) {
	printf("bx%s    %s",
		condition_suffixes[w>>28],
		reg_names[w&0xF]
	);
}

/* A8.6.26 BXJ */
/* A8.6.27 CBNZ, CBZ */
/* A8.6.28 CDP, CDP2 */
/* A8.6.29 CHKA */
/* A8.6.30 CLREX */
/* A8.6.31 CLZ */
/* A8.6.32 CMN (immediate) */
/* A8.6.33 CMN (register) */
/* A8.6.34 CMN (register-shifted register) */

/* A8.6.35 CMP (immediate) */
void decode_cmp_imm(unsigned long w) {
	printf("cmp%s   %s, #%ld", condition_suffixes[w>>28],
		reg_names[(w>>16)&0xF],
		w&0x3F
	);
}

/* A8.6.36 CMP (register) */
void decode_cmp_reg(unsigned long w) {
	printf("cmp%s   %s, %s, %lx", condition_suffixes[w>>28],
		reg_names[(w>>16) & 0xF],
		reg_names[w&0xF],
		(w>>7)&0x1F
	);
}

/* A8.6.37 CMP (register-shifted register) */
/* A8.6.38 CPS */
/* A8.6.39 CPY */
/* A8.6.40 DBG */
/* A8.6.41 DMB */
/* A8.6.42 DSB */
/* A8.6.43 ENTERX */
/* A8.6.44 EOR (immediate) */
/* A8.6.45 EOR (register) */
/* A8.6.46 EOR (register-shifted register) */
/* A8.6.47	F* (former VFP instruction mnemonics) */
/* A8.6.48	HB, HBL, HBLP, HBP */
/* A8.6.49 ISB */
/* A8.6.50 IT */
/* A8.6.51 LDC, LDC2 (immediate) */
/* A8.6.52 LDC, LDC2 (literal) */
/* A8.6.53 LDM / LDMIA / LDMFD */
/* A8.6.54 LDMDA / LDMFA */
/* A8.6.55 LDMDB / LDMEA */
/* A8.6.56 LDMIB / LDMED */
/* A8.6.57 LDR (immediate, Thumb) */

/* A8.6.58 LDR (immediate, ARM) */
void decode_ldr_imm(unsigned long w) {
	printf("ldr%s   %s, [%s, #-%ld]", condition_suffixes[w>>28],
		reg_names[(w>>12) & 0xF],
		reg_names[(w>>16) & 0xF],
		w&0xFFF);
}

/* A8.6.59 LDR (literal) */
void decode_ldr_literal(unsigned long w) {
	printf("ldr%s   %s, [pc, #%ld]", condition_suffixes[w>>28],
		reg_names[(w>>12) & 0xF], w & 0x3F);
}

/* A8.6.60 LDR (register) */
void decode_ldr_reg(unsigned long w) {
	printf("ldr%s   %s, %s, %s", condition_suffixes[w>>28],
		reg_names[(w>>12) & 0xF],
		reg_names[(w>>16) & 0xF],
		reg_names[w&0xF]);
}

/* A8.6.62 LDRB (immediate, ARM) */
void decode_ldrb_imm(unsigned long w) {
	printf("ldrb%s   %s, %s, #%ld", condition_suffixes[w>>28],
		reg_names[(w>>12) & 0xF],
		reg_names[(w>>16) & 0xF],
		w&0x3F);
}

/* A8.6.96 MOV (immediate) */
void decode_mov_imm(unsigned long w) {
	printf("mov%s   %s, #%ld", condition_suffixes[w>>28],
		reg_names[(w>>12) & 0xF], w & 0xFF);
}

/* A8.6.97 MOV (register) */
void decode_mov_reg(unsigned long w) {
	printf("mov%s   %s, r%ld", condition_suffixes[w>>28],
		reg_names[(w>>12) & 0xF], w & 0xF);
}

/* A8.6.105 MUL */
void decode_mul(unsigned long w) {
	/* b20 = update flags */
	printf("mul%s   %s, %s, %s", condition_suffixes[w>>28],
		reg_names[(w>>16) & 0xF],
		reg_names[w&0xF],
		reg_names[(w>>8) & 0xF]);
}

/* A8.6.122 POP */
void decode_pop_reg_list(unsigned long w) {
	int count=0;
	int i;
	printf("pop%s   {", condition_suffixes[w>>28]);
	for(i=0;i<16;i++) if((w>>i)&0x1) {
		if(count) printf(", ");
		printf("%s", reg_names[i]);
		++count;
	}
	printf("}");
}

/* A8.6.122 POP */
void decode_pop_one_reg(unsigned long w) {
	printf("pop%s  %s", condition_suffixes[w>>28],
		reg_names[(w>>12) & 0xF]);
}

/* A8.6.123 PUSH */
void decode_push_reg_list(unsigned long w) {
	int count=0;
	int i;
	printf("push%s  {", condition_suffixes[w>>28]);
	for(i=0;i<16;i++) if((w>>i)&0x1) {
		if(count) printf(", ");
		printf("%s", reg_names[i]);
		++count;
	}
	printf("}");
}

/* A8.6.123 PUSH */
void decode_push_one_reg(unsigned long w) {
	printf("push%s  %s", condition_suffixes[w>>28],
		reg_names[(w>>12) & 0xF]);
}

/* A8.6.194 STR (immediate, ARM) */
void decode_str(unsigned long w) {
	int imm12;
	imm12 = w & 0x3F;
	printf("str%s   %s, [%s", condition_suffixes[w>>28],
		reg_names[(w>>12)&0xF], reg_names[(w>>16)&0xF]
	);
	if(imm12) printf(", #-%d", imm12);
	printf("]");
}

/* A8.6.210 STRT: Encoding A1 */
void decode_strt(unsigned long w) {
	printf("strt%s   %s, %s, #%ld", condition_suffixes[w>>28],
			reg_names[(w>>12) & 0xF],
			reg_names[(w>>16) & 0xF],
			w&0xFFF
	);
}

/* A8.6.210 STRT: Encoding A2 */
void decode_strt_shift(unsigned long w) {
	printf("strt%s   %s, %s, +%s, #%ld", condition_suffixes[w>>28],
			reg_names[(w>>12) & 0xF],
			reg_names[(w>>16) & 0xF],
			reg_names[w&0xF],
			(w>>7)&0x1F
	);
}

/* A8.6.213 SUB (register) */
void decode_sub_reg(unsigned long w) {
	if( ((w>>4) & 0x01) == 0) {
		printf("sub%c%s  %s, %s, %s, # %ld",
			((w>>20)&0x1) ? 's':' ', condition_suffixes[w>>28],
			reg_names[(w>>12) & 0xF],
			reg_names[(w>>16) & 0xF],
			reg_names[w&0xF],
			(w>>7) & 0x1F
		);
	} else {
		/* A8.6.214 SUB (register-shifted register) */
		printf("sub%c%s  %s, %s, %s, %s %s",
			((w>>20)&0x1) ? 's':' ', condition_suffixes[w>>28],
			reg_names[(w>>12) & 0xF],
			reg_names[(w>>16) & 0xF],
			reg_names[w&0xF],
			shift_types[(w>>5) & 0x03],
			reg_names[(w>>8) & 0xF]
		);
	}
}

/* A8.6.214 SUB (register-shifted register) */
void decode_sub_reg_shift_reg(unsigned long w) {
		/* A8.6.7 ADD (register-shifted register) */
		printf("sub%c%s  %s, %s, %s, %s %s",
			((w>>20)&0x1) ? 's':' ', condition_suffixes[w>>28],
			reg_names[(w>>12) & 0xF],
			reg_names[(w>>16) & 0xF],
			reg_names[w&0xF],
			shift_types[(w>>5) & 0x03],
			reg_names[(w>>8) & 0xF]
		);
	
}
/* A8.6.215 SUB (SP minus immediate) */
void decode_sub_sp_minus_imm(unsigned long w) {
        printf("sub%c%s  %s, sp, # %ld",
                ((w>>20)&0x1) ? 's':' ', condition_suffixes[w>>28],
                reg_names[(w>>12) & 0xF],
                w&0xFFF
        );
}

/* divinado from A8.6.215, not acutally documented in the A.R.M.! */
void decode_sub_fp_minus_imm(unsigned long w) {
        printf("sub%c%s  %s, fp, #%ld",
                ((w>>20)&0x1) ? 's':' ', condition_suffixes[w>>28],
                reg_names[(w>>12) & 0xF],
                w&0xFFF
        );
}

/* A8.6.218 SVC (previously SWI) */
void decode_svc(unsigned long w) {
	printf("svc%s  %lx", condition_suffixes[w>>28],
		w&0xFFF);
}

typedef struct {
	unsigned long offset, mask, value;
	void (*decode_func)(unsigned long);
} bitmask;
/* The full table starts at A5.1 ARM instruction set encoding
   WARNING: the order of the following bitmasks MATTERS, as more specific bitmasks need to be evaluated
   before more inclusive masks.
*/
bitmask bitmasks[] = {
	{ 0,  0x0C500000, 0x04000000, decode_str}, /* 01ax x0x0 */
	{21,  0x7F,  0x00, decode_mul},
	{ 0,  0x0F000000, 0x0A000000, decode_b},
	{21,  0x7F,  0x1D, decode_mov_imm},
	{21,  0x7F,  0x0D, decode_mov_reg},
	{ 0,  0x0FFF0000, 0x092D0000, decode_push_reg_list},
	{ 0,  0x0FFF0000, 0x052D0000, decode_push_one_reg},
	{ 0,  0x0FFF0000, 0x08BD0000, decode_pop_reg_list},
	{ 0,  0x0FFF0000, 0x049D0000, decode_pop_one_reg},
	{ 0,  0x0F7F0000, 0x051F0000, decode_ldr_literal},
	{ 0,  0x0E500000, 0x04500000, decode_ldrb_imm},
	{ 0,  0x0E500000, 0x06100000, decode_ldr_reg},
	{ 0,  0x0FF00000, 0x03500000, decode_cmp_imm},
	{ 0,  0x0FF00000, 0x01500000, decode_cmp_reg},
	{ 0,  0x0FEF0000, 0x28D0000, decode_add_sp_plus_imm},
	{21,  0x7F,   0x2, decode_sub_reg},
	{ 0,  0x0FEF0000, 0x024D0000, decode_sub_sp_minus_imm},
	{ 0,  0x0FEF0000, 0x024B0000, decode_sub_fp_minus_imm},  /* ems UNDOCUMENTED FEATURE */
	{ 0,  0x0FE00000, 0x00400000, decode_sub_reg_shift_reg},
	{ 0,  0x0E500000, 0x04100000, decode_ldr_imm},
	{ 0,  0x0FFF0000, 0x024F0000, decode_adr_pc_zero},
	{ 0,  0x0F000000, 0x0F000000, decode_svc},
	{ 0,  0x0F700000, 0x04200000, decode_strt},
	{ 0,  0x0F600010, 0x06200000, decode_strt_shift},

	{ 0,  0x0FE00000, 0x02C00000, decode_adc_imm},
	{ 0,  0x0FE00000, 0x00C00000, decode_adc_reg},
	{ 0,  0x0FE00000, 0x02800000, decode_add_imm},
	{ 0,  0x0FE00000, 0x00800000, decode_add_reg},
	{ 0,  0x0FEF0010, 0x008D0000, decode_add_sp_plus_reg },
	{ 0,  0x0FFF0000, 0x028F0000, decode_adr},
	{ 0,  0x0FE00000, 0x02000000, decode_and_imm },
	{ 0,  0x0FE00010, 0x00000000, decode_and_reg },
	{ 0,  0x0FEF0070, 0x01A00040, decode_asr_imm },
	{ 0,  0x0FEF00F0, 0x01A00050, decode_asr_reg },
	{ 0,  0x0FE00070, 0x07C00010, decode_bfi },
	{ 0,  0x0FE0007F, 0x07C0001F, decode_bfc },
	{ 0,  0x0FE00000, 0x03C00000, decode_bic_imm },
	{ 0,  0x0FE00010, 0x01C00000, decode_bic_reg },
	{ 0,  0x0FE00090, 0x01C00010, decode_bic_reg_shift_reg },
        { 0,  0x0FF000F0, 0x01200070, decode_bkpt_imm },
	{ 0,  0x0E000000, 0x0B000000, decode_bl_imm },
	{ 0,  0x0FFFFFF0, 0x012FFF30, decode_blx_reg },
	{ 0,  0x0FFFFFF0, 0x012FFF10, decode_bx_reg },

	{0,0,0, NULL}
};



void decode_armv6(unsigned long w) {
	int i;
	bitmask *b;
	int found;

	printf(" ");
	for(i=31;i>=24;i--)
		printf("%c", BIT(w,i)?'1':'0');
	printf(".");
	for(i=23;i>=16;i--)
		printf("%c", BIT(w,i)?'1':'0');
	printf(".");
	for(i=15;i>=8;i--)
		printf("%c", BIT(w,i)?'1':'0');
	printf(".");
	for(i=7;i>=0;i--)
		printf("%c", BIT(w,i)?'1':'0');

	found=0;
	for(b=bitmasks;b->decode_func && !found;b++) {
		if( ((w>>b->offset) & b->mask) == b->value ) {
			printf(" ");
			if(b->decode_func) b->decode_func(w);
			found=1;
			break;
		}

	}
	if(!found) printf("; <UNDEFINED> instruction: 0x%08lx\n", w);
}
